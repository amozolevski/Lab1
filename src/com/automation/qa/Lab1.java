/* 1. Написати програму для роботи зi списком.
У першій половині списку замінити всі входження деякого елементу Е1 на  будь-який інший елемент Е2;
Заменить 3 элемент на 8.
 */

package com.automation.qa;

import java.util.ArrayList;
import java.util.List;

public class Lab1 {

    private static List<String> myList = new ArrayList<>();
    static {
        for (int i = 0; i < 10; i++) {
            myList.add("Name" + i);
        }
    }

    public static void main(String[] args) {
        changeList3on8(myList);
        printList(myList);
    }

    private static List<String> changeList3on8(List<String> list){
        String buffer = list.get(2);
        list.set(2, list.get(7));
        list.set(7, buffer);

        return list;
    }

    private static void printList(List<String> list){
        System.out.println("Changed list:");
        for (String s : list) {
            System.out.println(s);
        }
    }

}
